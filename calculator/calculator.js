/* My Calculator JS */
const calScreen = document.querySelector(".result-box");
const allButtons = document.querySelector(".keys-wrapper");
let buffer = "0";
let runningTotal = 0;
let previousOperation = null;

allButtons.addEventListener("click", function (event){
    buttonClick(event.target.innerText);
});

function buttonClick(value){
    if(isNaN(value)){
        handleOperation(value);
    }else{
        handleNumber(value);
    }
    rerender();
}

function handleNumber(value){
    if(buffer === "0"){
        buffer = value;
    }else{
        buffer += value;
    }
}

function handleOperation(value){
    switch (value){
        case '∁':
            buffer = "0";
            runningTotal = 0;
            previousOperation = null;
            break;
        case '=':
            if(previousOperation === null){
                return;
            }
            flushOperation(parseInt(buffer));
            previousOperation = null;
            buffer = "" + runningTotal;
            runningTotal = 0;
            break;
        case '←':
            if(buffer.length === 1){
                buffer = "0";
            }else{
                buffer = buffer.substring(0, buffer.length - 1);
            }
            break;
        default:
            handleMath(value);
            break;
    }
}

function handleMath(value){
    const intBuffer = parseInt(buffer);

    if(runningTotal === 0){
        runningTotal = intBuffer;
    }else{
        flushOperation(intBuffer);
    }
    previousOperation = value;
    buffer = "0";
}

function flushOperation(intBuffer){
    if(previousOperation === "+"){
        runningTotal += intBuffer;
    }else if(previousOperation === "−"){
        runningTotal -= intBuffer;
    }else if(previousOperation === "×"){
        runningTotal *= intBuffer;
    }else{
        runningTotal /= intBuffer;
    }
}

function rerender(){
    calScreen.innerText = buffer;
}




